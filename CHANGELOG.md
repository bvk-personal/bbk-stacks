# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.10](https://github.com/brickblock/bbk-stacks/compare/v0.0.7...v0.0.10) (2019-08-16)


### Bug Fixes

* compose working directory ([e52a131](https://github.com/brickblock/bbk-stacks/commit/e52a131))
* hard code container name ([c65650d](https://github.com/brickblock/bbk-stacks/commit/c65650d))

### [0.0.7](https://github.com/brickblock/bbk-stacks/compare/v0.0.5...v0.0.7) (2019-08-16)


### Bug Fixes

* compose working directory ([6247c6c](https://github.com/brickblock/bbk-stacks/commit/6247c6c))

### [0.0.5](https://github.com/brickblock/bbk-stacks/compare/v0.0.2...v0.0.5) (2019-08-16)

### 0.0.2 (2019-08-16)


### Bug Fixes

* include compose file ([dacc3ab](https://github.com/brickblock/bbk-stacks/commit/dacc3ab))
* no need to build the package ([aa54b7e](https://github.com/brickblock/bbk-stacks/commit/aa54b7e))


### Features

* initial commit ([107e153](https://github.com/brickblock/bbk-stacks/commit/107e153))
