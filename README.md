@brickblock/bbk-stacks
======================

Brickblock Stacks

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/@brickblock/bbk-stacks.svg)](https://npmjs.org/package/@brickblock/bbk-stacks)
[![Downloads/week](https://img.shields.io/npm/dw/@brickblock/bbk-stacks.svg)](https://npmjs.org/package/@brickblock/bbk-stacks)
[![License](https://img.shields.io/npm/l/@brickblock/bbk-stacks.svg)](https://github.com/brickblock/bbk-stacks/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g @barath/bbk-stacks
$ stacks COMMAND
running command...
$ stacks (-v|--version|version)
@barath/bbk-stacks/0.0.9 linux-x64 node-v12.6.0
$ stacks --help [COMMAND]
USAGE
  $ stacks COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`stacks create STACK`](#stacks-create-stack)
* [`stacks delete STACK`](#stacks-delete-stack)
* [`stacks deploy [TAG]`](#stacks-deploy-tag)
* [`stacks help [COMMAND]`](#stacks-help-command)
* [`stacks list`](#stacks-list)
* [`stacks select [STACK]`](#stacks-select-stack)
* [`stacks service:add [STACK]`](#stacks-serviceadd-stack)
* [`stacks service:list [PROJECT]`](#stacks-servicelist-project)
* [`stacks service:remove [ENDPOINT]`](#stacks-serviceremove-endpoint)
* [`stacks start`](#stacks-start)
* [`stacks stop`](#stacks-stop)

## `stacks create STACK`

create a new stack

```
USAGE
  $ stacks create STACK

OPTIONS
  -d, --description=description  (required)
```

_See code: [src/commands/create.ts](https://github.com/brickblock/bbk-stacks/blob/v0.0.9/src/commands/create.ts)_

## `stacks delete STACK`

delete a stack

```
USAGE
  $ stacks delete STACK
```

_See code: [src/commands/delete.ts](https://github.com/brickblock/bbk-stacks/blob/v0.0.9/src/commands/delete.ts)_

## `stacks deploy [TAG]`

deploy the current application to Google Cloud Run

```
USAGE
  $ stacks deploy [TAG]

OPTIONS
  -p, --project=project              project to deploy into
  -s, --skip=build|push|deploy|info
```

_See code: [src/commands/deploy.ts](https://github.com/brickblock/bbk-stacks/blob/v0.0.9/src/commands/deploy.ts)_

## `stacks help [COMMAND]`

display help for stacks

```
USAGE
  $ stacks help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.1/src/commands/help.ts)_

## `stacks list`

list all stacks

```
USAGE
  $ stacks list
```

_See code: [src/commands/list.ts](https://github.com/brickblock/bbk-stacks/blob/v0.0.9/src/commands/list.ts)_

## `stacks select [STACK]`

select a stack

```
USAGE
  $ stacks select [STACK]
```

_See code: [src/commands/select.ts](https://github.com/brickblock/bbk-stacks/blob/v0.0.9/src/commands/select.ts)_

## `stacks service:add [STACK]`

add a service to a stack

```
USAGE
  $ stacks service:add [STACK]

OPTIONS
  -e, --endpoint=endpoint  path on the local server to map the Google Cloud Run service
  -p, --port=port          local port to proxy
  -s, --service=service    Google Cloud Run service to point to
  -s, --strip              strip the path prefix
  --project=project        Google Cloud project of the service
```

_See code: [src/commands/service/add.ts](https://github.com/brickblock/bbk-stacks/blob/v0.0.9/src/commands/service/add.ts)_

## `stacks service:list [PROJECT]`

describe the command here

```
USAGE
  $ stacks service:list [PROJECT]

OPTIONS
  -f, --force
  -h, --help       show CLI help
  -n, --name=name  name to print
```

_See code: [src/commands/service/list.ts](https://github.com/brickblock/bbk-stacks/blob/v0.0.9/src/commands/service/list.ts)_

## `stacks service:remove [ENDPOINT]`

add a service to a stack

```
USAGE
  $ stacks service:remove [ENDPOINT]

OPTIONS
  -s, --stack=stack  stack to remove the service from
```

_See code: [src/commands/service/remove.ts](https://github.com/brickblock/bbk-stacks/blob/v0.0.9/src/commands/service/remove.ts)_

## `stacks start`

start the stacks proxy

```
USAGE
  $ stacks start

OPTIONS
  -p, --port=port  [default: 1234] Port for the the Stacks server to listen on
```

_See code: [src/commands/start.ts](https://github.com/brickblock/bbk-stacks/blob/v0.0.9/src/commands/start.ts)_

## `stacks stop`

stop the stacks proxy

```
USAGE
  $ stacks stop
```

_See code: [src/commands/stop.ts](https://github.com/brickblock/bbk-stacks/blob/v0.0.9/src/commands/stop.ts)_
<!-- commandsstop -->
