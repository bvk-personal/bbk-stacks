import { Command, flags } from '@oclif/command'
import { createStack } from '../lib/stacks'
export default class Create extends Command {
  static description = 'create a new stack'
  static flags = {
    description: flags.string({
      char: 'd',
      required: true,
    }),
  }
  static args = [{ name: 'stack', required: true }]

  async run() {
    const parsed = this.parse(Create)
    await createStack(parsed.args.stack, parsed.flags.description)
  }
}
