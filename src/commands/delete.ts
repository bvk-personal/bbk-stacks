import { Command } from '@oclif/command'
import { deleteStack } from '../lib/stacks'
export default class Delete extends Command {
  static description = 'delete a stack'
  static args = [{ name: 'stack', required: true }]

  async run() {
    const parsed = this.parse(Delete)
    await deleteStack(parsed.args.stack)
  }
}
