import { Command, flags } from '@oclif/command'
import { getCurrentProject } from '../lib/gcloud'
import path from 'path'
import branch from 'git-branch'
import ora = require('ora')
import { execute } from '../lib/execute'
import slugify from 'slugify'
import fs from 'fs-extra'
export default class Deploy extends Command {
  static description = 'deploy the current application to Google Cloud Run'

  static flags = {
    project: flags.string({ char: 'p', description: 'project to deploy into' }),
    skip: flags.string({
      char: 's',
      multiple: true,
      options: ['build', 'push', 'deploy', 'info'],
    }),
  }

  static args = [{ name: 'tag' }]

  async run() {
    const parsed = this.parse(Deploy)
    if (!parsed.flags.project) {
      parsed.flags.project = await getCurrentProject()
    }
    const spinner = ora(`Deploying to project ${parsed.flags.project}`)
    if (!parsed.args.tag) {
      const currentBranch = fs.existsSync('.git')
        ? await branch().catch(error => {
            spinner.warn(error.message)
            spinner.warn('The build will be tagged as `latest`')
          })
        : 'unknown'
      const tag = currentBranch === 'master' ? 'latest' : currentBranch
      parsed.args.tag = `${path.basename(process.cwd())}:${tag}`
    }

    const serviceName = slugify(parsed.args.tag, {
      lower: true,
    }).replace(/:/g, '-')

    if (!parsed.flags.skip || !parsed.flags.skip.includes('build')) {
      spinner.info(`Building Docker image...`)
      await execute(
        `docker build . --tag gcr.io/${parsed.flags.project}/${parsed.args.tag}`
      )
    }
    if (!parsed.flags.skip || !parsed.flags.skip.includes('push')) {
      spinner.info(`Pushing Docker image...`)
      await execute(
        `docker push gcr.io/${parsed.flags.project}/${parsed.args.tag}`,
        true
      )
    }
    if (!parsed.flags.skip || !parsed.flags.skip.includes('deploy')) {
      spinner.info(`Deploying to Google Cloud Run`)
      await execute(
        `gcloud beta run deploy ${serviceName} --image gcr.io/${parsed.flags.project}/${parsed.args.tag} --project ${parsed.flags.project} --allow-unauthenticated --platform managed --quiet`,
        true
      )
    }
    if (!parsed.flags.skip || !parsed.flags.skip.includes('info')) {
      const url = await execute(
        `gcloud beta run services describe ${serviceName} --format json --platform managed | jq .status.url -r`
      )
      spinner.info(`Project is deployed at ${url}`)
      spinner.info(
        `Logs are available at https://console.cloud.google.com/run/detail/europe-west1/${serviceName}/logs?project=${parsed.flags.project}`
      )
    }
  }
}
