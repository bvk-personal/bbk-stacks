import { Command } from '@oclif/command'
import ora = require('ora')
import { listStacks } from '../lib/stacks'
export default class List extends Command {
  static description = 'list all stacks'
  async run() {
    const spinner = ora('Listing stacks...')
    spinner.start()
    const stacks = await listStacks()
    stacks.forEach(stack => {
      spinner.info(`${stack.current ? '[*] ' : ''}${stack.name}`)
    })
    spinner.succeed(`Found ${stacks.length} stacks`)
  }
}
