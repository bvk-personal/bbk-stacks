import { Command } from '@oclif/command'
import { selectStack } from '../lib/stacks'

export default class Select extends Command {
  static description = 'select a stack'

  static args = [{ name: 'stack' }]

  async run() {
    const parsed = this.parse(Select)
    await selectStack(parsed.args.stack)
  }
}
