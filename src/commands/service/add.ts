import { Command, flags } from '@oclif/command'
import {
  getCurrentStack,
  getStack,
  Service,
  updateStack,
} from '../../lib/stacks'
import ora = require('ora')
import { getCurrentProject } from '../../lib/gcloud'

export default class ServiceAdd extends Command {
  static description = 'add a service to a stack'

  static flags = {
    service: flags.string({
      char: 's',
      description: 'Google Cloud Run service to point to',
      dependsOn: ['endpoint', 'project'],
    }),
    endpoint: flags.string({
      char: 'e',
      description:
        'path on the local server to map the Google Cloud Run service',
    }),
    port: flags.integer({
      char: 'p',
      description: 'local port to proxy',
      exclusive: ['service'],
    }),
    strip: flags.boolean({
      char: 's',
      description: 'strip the path prefix',
      default: true,
    }),
    project: flags.string({
      description: 'Google Cloud project of the service',
    }),
  }

  static args = [{ name: 'stack' }]

  async run() {
    const parsed = this.parse(ServiceAdd)
    if (!parsed.args.stack) {
      parsed.args.stack = await getCurrentStack().then(stack =>
        stack ? stack.name : undefined
      )
    }
    if (!parsed.flags.project) {
      parsed.flags.project = await getCurrentProject()
    }
    const spinner = ora(
      `Adding service ${parsed.flags.endpoint} to stack ${parsed.args.stack}...`
    ).start()
    const stack = await getStack(parsed.args.stack)
    if (stack.services.find(svc => svc.endpoint === parsed.flags.endpoint)) {
      spinner.fail(
        `Service ${parsed.flags.endpoint} already exists in stack ${parsed.args.stack}`
      )
    } else {
      const service = new Service()
      service.endpoint = parsed.flags.endpoint!
      service.port = parsed.flags.port!
      service.service = parsed.flags.service
      service.strip = parsed.flags.strip
      service.project = parsed.flags.project
      stack.services.push(service)
      await updateStack(stack)
      spinner.succeed()
    }
  }
}
