import { Command, flags } from '@oclif/command'
import { getCurrentProject, listServices } from '../../lib/gcloud'
import ora = require('ora')

export default class ServiceList extends Command {
  static description = 'describe the command here'

  static flags = {
    help: flags.help({ char: 'h' }),
    // flag with a value (-n, --name=VALUE)
    name: flags.string({ char: 'n', description: 'name to print' }),
    // flag with no value (-f, --force)
    force: flags.boolean({ char: 'f' }),
  }

  static args = [{ name: 'project' }]

  async run() {
    const parsed = this.parse(ServiceList)
    if (!parsed.args.project) {
      parsed.args.project = await getCurrentProject()
    }
    const spinner = ora(
      `Getting services from project ${parsed.args.project}`
    ).start()
    const services = await listServices(parsed.args.project)
    services.forEach(service =>
      spinner.info(`${service.metadata.name}: ${service.status.url}`)
    )
    spinner.succeed(`Found ${services.length} services`)
  }
}
