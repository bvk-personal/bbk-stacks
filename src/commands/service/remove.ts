import { Command, flags } from '@oclif/command'
import { getCurrentStack, getStack, updateStack } from '../../lib/stacks'
import ora = require('ora')

export default class ServiceRemove extends Command {
  static description = 'add a service to a stack'
  static flags = {
    stack: flags.string({
      char: 's',
      description: 'stack to remove the service from',
    }),
  }
  static args = [{ name: 'endpoint' }]

  async run() {
    const parsed = this.parse(ServiceRemove)
    const currentStack = await getCurrentStack().then(stack =>
      stack ? stack.name : undefined
    )
    if (!parsed.flags.stack) {
      parsed.flags.stack = currentStack
    }
    const spinner = ora(
      `Removing service ${parsed.args.endpoint} to stack ${parsed.flags.stack}...`
    ).start()
    const stack = await getStack(parsed.flags.stack!)
    const service = stack.services.find(
      svc => svc.endpoint === parsed.args.endpoint
    )
    if (!service) {
      spinner.fail(
        `Service ${parsed.args.endpoint} does not exist in stack ${parsed.args.stack}`
      )
    } else {
      stack.services = stack.services.filter(
        svc => svc.endpoint !== parsed.args.endpoint
      )
      if (currentStack === parsed.flags.stack) {
        await updateStack(stack)
      }
      spinner.succeed()
    }
  }
}
