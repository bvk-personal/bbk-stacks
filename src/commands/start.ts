import { Command, flags } from '@oclif/command'
import { execute } from '../lib/execute'
export default class Start extends Command {
  static description = 'start the stacks proxy'
  static flags = {
    port: flags.integer({
      char: 'p',
      description: 'Port for the the Stacks server to listen on',
      default: 1234,
    }),
  }
  async run() {
    const parsed = this.parse(Start)
    await execute(
      `env PORT=${parsed.flags.port} docker-compose up -d --remove-orphans`,
      false,
      `Starting Stacks server on port ${parsed.flags.port}...`,
      __dirname
    )
  }
}
