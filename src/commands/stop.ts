import { Command } from '@oclif/command'
import { execute } from '../lib/execute'
export default class Stop extends Command {
  static description = 'stop the stacks proxy'

  async run() {
    const port = await execute(
      `docker inspect bbk-stacks_traefik_1 | jq '.[0].HostConfig.PortBindings."80/tcp"[0].HostPort' -r`,
      true,
      'Getting Stacks server port...'
    )
    await execute(
      `env PORT=${port} docker-compose down`,
      false,
      'Stopping Stacks server...',
      __dirname
    )
  }
}
