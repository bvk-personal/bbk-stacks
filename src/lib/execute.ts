import ora from 'ora'
import { exec } from 'shelljs'
export const execute = async (
  command: string,
  silent = false,
  message = `Running ${command}`,
  cwd: string = process.cwd()
): Promise<string> => {
  const promise: Promise<string> = new Promise((resolve, reject) => {
    const spinner = ora(message).start()
    const child = exec(
      command,
      { async: true, cwd, silent: true },
      (code, stdout, stderr) => {
        if (code !== 0) {
          spinner.fail(stderr)
          reject(stderr.trim())
        } else {
          spinner.succeed()
          resolve(stdout.trim())
        }
      }
    )
    if (!silent) {
      child.stdout.on('data', data => {
        spinner.info(data)
      })
      child.stderr.on('data', (data: string) => {
        const lines = data.split('\r\n')
        lines
          .map(line => line.trim().replace(/\s/g, ''))
          .forEach(line => {
            if (line.length > 0) {
              spinner.info(line)
            }
          })
      })
    }
  })

  return promise
}
