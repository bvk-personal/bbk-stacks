import { execute } from './execute'
export const listServices = async (project: string) => {
  return execute(
    `gcloud beta run services list --platform managed --format json --project ${project} --quiet | jq .`,
    true,
    'Retrieving list of services...'
  ).then(output => JSON.parse(output))
}

export const getCurrentUser = async () => {
  return execute(
    'gcloud config list account --format json | jq .core.account -r',
    true,
    'Getting current user...'
  )
}

export const getService = async (service: string, project: string) => {
  return execute(
    `gcloud beta run services describe ${service} --project ${project} --format json --platform managed | jq .status.url -r`,
    true,
    `Getting service ${service} in project ${project}...`
  )
}

export const getCurrentProject = async () => {
  return execute(
    'gcloud config get-value project --format json | jq . -r',
    true,
    'Getting current project...'
  )
}
