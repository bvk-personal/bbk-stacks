import fs from 'fs-extra'
import { plainToClass } from 'class-transformer'
import {
  IsDefined,
  validate,
  ValidationError,
  IsOptional,
  IsNumber,
  Max,
  Min,
  ValidateNested,
} from 'class-validator'
import ora from 'ora'
import { getCurrentUser, getService } from './gcloud'
import { execute } from './execute'
import axios from 'axios'
import delay from 'delay'
const stackFile = `${process.cwd()}/stacks.json`

export class Service {
  @IsDefined()
  public endpoint: string

  @IsDefined()
  public strip: boolean

  @IsOptional()
  public service?: string

  @IsOptional()
  public project?: string

  @IsOptional()
  @IsNumber()
  @Max(65536)
  @Min(1024)
  public port: number
}

export class Stack {
  @IsDefined()
  public name: string

  @IsDefined()
  public current: boolean

  @IsDefined()
  public description: string

  @IsDefined()
  public updatedBy: string

  @IsDefined()
  public createdBy: string

  @IsDefined()
  public updatedAt = new Date()

  @IsDefined()
  public createdAt = new Date()

  @IsDefined()
  @ValidateNested({ each: true })
  public services: Service[]
}

const getMessages = (messages: string[], error: ValidationError) => {
  if (error.constraints) {
    Object.values(error.constraints).forEach(msg => messages.push(msg))
  }
  if (error.children) {
    error.children.forEach(child => getMessages(messages, child))
  }
}

export const listStacks = async (): Promise<Stack[]> => {
  const exists = await fs.existsSync(stackFile)
  if (exists) {
    return fs
      .readJson(stackFile)
      .then(stacks => stacks.map((stack: any) => plainToClass(Stack, stack)))
  } else {
    return []
  }
}

export const createStack = async (name: string, description: string) => {
  const user = await getCurrentUser()
  const spinner = ora(`Creating stack ${name}...`)
  const stacks = await listStacks()
  if (stacks.find(stack => stack.name === name)) {
    spinner.fail(`A stack named ${name} already exists`)
  } else {
    const newStack = new Stack()
    newStack.name = name
    newStack.description = description
    newStack.current = false
    newStack.createdBy = user
    newStack.updatedBy = user
    newStack.services = []
    const errors = await validate(newStack)
    if (errors.length > 0) {
      const messages: string[] = []
      errors.forEach(error => getMessages(messages, error))
      messages.forEach(message => spinner.fail(message))
    } else {
      stacks.push(newStack)
      await fs.writeJSON(stackFile, stacks, { spaces: 2 })
      spinner.succeed()
    }
  }
}

export const deleteStack = async (name: string) => {
  const spinner = ora(`Deleting stack ${name}...`)
  const stacks = await listStacks()
  if (!stacks.find(stack => stack.name === name)) {
    spinner.fail(`A stack named ${name} does not exist`)
  } else {
    const newStacks = stacks.filter(stack => stack.name !== name)
    await fs.writeJSON(stackFile, newStacks, { spaces: 2 })
    spinner.succeed()
  }
}

export const selectStack = async (name: string) => {
  const user = await getCurrentUser()
  const spinner = ora(`Selecting stack ${name}...`)
  const stacks = await listStacks()
  if (!stacks.find(stack => stack.name === name)) {
    spinner.fail(`A stack named ${name} does not exist`)
  } else {
    stacks.forEach(stack => (stack.current = false))
    const stack = stacks.find(stack => stack.name === name)
    stack!.current = true
    stack!.updatedBy = user
    stack!.updatedAt = new Date()
    await fs.writeJSON(stackFile, stacks, { spaces: 2 })
    spinner.succeed()
  }
}

export const getCurrentStack = async () => {
  const stacks = await listStacks()
  const stack = stacks.find(stack => stack.current)
  return stack
}

export const updateStack = async (updatedStack: Stack) => {
  const user = await getCurrentUser()
  const port = await execute(
    `docker inspect bbk-stacks_traefik_1 | jq '.[0].HostConfig.PortBindings."80/tcp"[0].HostPort' -r`,
    true,
    'Getting Stacks server port... y'
  )
  const spinner = ora(`Updating stack ${updatedStack.name}...`)
  const stacks = await listStacks()
  if (!stacks.find(stack => stack.name === updatedStack.name)) {
    spinner.fail(`A stack named ${name} does not exist`)
  } else {
    const newStacks = stacks.filter(stack => stack.name !== updatedStack.name)
    updatedStack.updatedAt = new Date()
    updatedStack.updatedBy = user
    const errors = await validate(updatedStack)
    if (errors.length > 0) {
      const messages: string[] = []
      errors.forEach(error => getMessages(messages, error))
      messages.forEach(message => spinner.fail(message))
    } else {
      await execute(
        `env PORT=${port} docker-compose restart`,
        true,
        'Updating Stacks server...',
        __dirname
      )
      spinner.info('Waiting for server to start...')
      await delay(3000)
      newStacks.push(updatedStack)
      await fs.writeJSON(stackFile, newStacks, { spaces: 2 })
      await updateTraefik()
      spinner.succeed()
    }
  }
}

export const getStack = async (name: string) => {
  const stacks = await listStacks()
  const stack = stacks.find(stack => stack.name === name)
  if (!stack) {
    throw new Error(`A stack named ${name} does not exist`)
  }
  return stack
}

export const updateTraefik = async () => {
  const spinner = ora(`Updating stack...`)
  const stack = await getCurrentStack()
  if (stack) {
    const port = await execute(
      `docker inspect bbk-stacks_traefik_1 | jq '.[0].HostConfig.PortBindings."80/tcp"[0].HostPort' -r`,
      true,
      'Getting Stacks server port... x'
    )
    const client = axios.create({
      baseURL: `http://localhost:${port}`,
    })
    const payload = {
      backends: {},
      frontends: {},
    }
    for (const service of stack.services) {
      payload.backends[service.endpoint] = {
        servers: {
          default: {
            URL: service.service
              ? await getService(service.service, service.project!)
              : `http://dockerhost:${service.port}`,
          },
        },
      }
      payload.frontends[service.endpoint] = {
        routes: {
          all: {
            rule: `PathPrefix${service.strip ? 'Strip' : ''}:${
              service.endpoint
            }`,
          },
        },
        backend: service.endpoint,
      }
    }
    await client.put('/traefik/api/providers/rest', payload)
    spinner.succeed()
  } else {
    spinner.fail('No stack selected')
  }
}
